//
//  ViewController.swift
//  GitTest
//
//  Created by Mike Neirinck on 12/11/2019.
//  Copyright © 2019 appCradle. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonClicked(_ sender: UIButton) {
        print("You finished!")
    }

    
}

